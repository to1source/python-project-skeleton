try: 
	from setuptools import setup 
except ImportError:
	from distuils.core import setup 
config = {
	'description': 'My_Py_Project',
	'author': 'Joel Chu',
	'url': 'URL_TO_GET_AT_IT',
	'download_url': 'URL_TO_DOWNLOAD',
	'author_email': 'joelchu@foxmail.com',
	'version': '0.1',
	'install_require': ['nose'],
	'packages': ['NAME'],
	'scripts': [],
	'name': 'PROJECT_NAME' 
}

setup(**config) 
